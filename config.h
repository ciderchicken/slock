/* user and group to drop privileges to */
static const char *user  = "nobody";
static const char *group = "nogroup";

static const char *colorname[NUMCOLS] = {
	[INIT] =   "#282828",     /* after initialization */
	[INPUT] =  "#458588",   /* during input */
	[FAILED] = "#cc241d",   /* wrong password */
};

/* treat a cleared input like a wrong password (color) */
static const int failonclear = 1;

/* lock screen opacity */
static const float alpha = 0.4;


/* default message */
static const char * message = "UwU";
  
/* text color */
static const char * text_color = "#d65d0e";
  
/* text size (must be a valid size) */
static const char * font_name = "fixed";

